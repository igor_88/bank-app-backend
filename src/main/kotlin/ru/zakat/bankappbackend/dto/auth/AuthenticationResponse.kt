package ru.zakat.bankappbackend.dto.auth

data class AuthenticationResponse(
    val jwtToken: String
)
